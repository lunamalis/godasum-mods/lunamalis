REM a
@echo off&setlocal
REM echo %%~dp0 == "%~dp0"
REM echo %%~dpnx0 == "%~dpnx0"

SET parent=%~dp0
REM ECHO parent=%parent%
FOR %%a IN ("%parent:~0,-1%") DO SET grandparent=%%~dpa
REM ECHO grandparent=%grandparent%

echo NOTICE: If you enter a RELATIVE path, assume your current working directory is %grandparent%lib.
set /p godasumdlldir="Enter Godasum DLL directory (required): "
echo %godasumdlldir%

IF NOT EXIST "%grandparent%lib" (
	echo Making %grandparent%lib
	mkdir "%grandparent%lib"
)

cd lib
IF NOT EXIST %godasumdlldir%\NUL (
	echo Given directory does not seem to exist. %godasumdlldir%\NUL
	exit /b 1
)

IF NOT EXIST "%grandparent%lib\Godasum" (
	mklink /D Godasum %godasumdlldir%
)
cd ..

REM Echo blank line.
echo.
echo This directory should contain Godasum.csproj.
echo NOTICE: If you enter a RELATIVE path, assume your current working directory is (notice the lack of lib) %grandparent%.
set /p godasumsourcedir="Enter Godasum source directory (optional, press enter to skip): "
REM TODO: why does it say `ECHO is off.`?
echo %godasumsourcedir%

IF "%godasumsourcedir%" == "" (
	echo Skipping source dir link.
	exit /b 0
)

REM The .source file is used for people to find the source directory.
IF NOT EXIST %godasumsourcedir%\Godasum.csproj (
	echo Given directory does not contain `Godasum.csproj` file.
	exit /b 1
)

mklink /D %grandparent%Godasum %godasumsourcedir%\Godasum

REM REM grandparent ends with a backslash.
REM echo %grandparent%Godasum
REM REM The inputted one should *not* end with a backslash, so we'll add it here.
REM REM Though after testing it, it seemingly does not matter.
REM echo DLL dir is %godasumdlldir%\Godasum
REM echo Source dir is %godasumsourcedir%\Godasum

