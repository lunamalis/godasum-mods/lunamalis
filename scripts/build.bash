#!/usr/bin/env bash
SCRIPT_DIR="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
python3.7 "${SCRIPT_DIR}/_package/package.py" "$@"
