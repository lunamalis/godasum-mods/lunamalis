#!/usr/bin/env python3.7
from typing import *
import os
import zipfile
import json

import tempfile
import shutil
import subprocess

import sys


class Packager:

	DELETABLE_DLLS: List[str] = [
		"Godasum",
		"GodotSharp",
		"System"
	]

	_cached_mod_zip_name: Union[str, None] = None
	_cached_in_wsl: Union[bool, None] = None

	def __init__(self, debug: bool = True):
		self.ABSOLUTE_DIR = os.path.dirname(os.path.abspath(__file__))
		self.PACKAGER_DIR = self.ABSOLUTE_DIR
		self.SCRIPTS_DIR = os.path.dirname(self.PACKAGER_DIR)
		self.REPO_DIR = os.path.dirname(self.SCRIPTS_DIR)

		self.MODINFO_PATH = os.path.join(self.REPO_DIR, "modinfo.json")
		self.PATHS_PATH = os.path.join(self.REPO_DIR, "paths.json")
		self.BUILD_DIR = os.path.join(self.REPO_DIR, "build")
		self.EXPORT_DIR = os.path.join(self.BUILD_DIR, "export")

		if debug:
			self.MONO_BIN_DIR = os.path.join(self.REPO_DIR, ".mono", "temp", "bin", "Debug")
		else:
			self.MONO_BIN_DIR = os.path.join(self.REPO_DIR, ".mono", "temp", "bin", "Release")

	def start(self) -> 'Packager':
		self._read_modinfo()
		self._read_paths()

		self.EXPORT_ZIP_PATH = os.path.join(self.EXPORT_DIR, f"{self.modid.capitalize()}.zip")

		# self.export_godot()
		# self.clean_zip()
		# self.copy_to_mods()

		return self

	def package_zip(self):
		"""Package the exported zip file.

		This cleans out the zip file that is exported by Godot.

		It removes some things that it obviously doesn't need, like the
		Godasum.dll file. These things should be provided by Godasum itself.

		It also packages the zip into a folder located in
		%APPDATA%/Roaming/Godasum/mods/{mod id}.

		It will add the modinfo.json as well as the dll files.
		"""

		# Make the directory first.
		mod_user_path: str = os.path.join(self.GODASUM_MODS_DIR, self.modid.capitalize())
		os.makedirs(mod_user_path, exist_ok=True)

		# export_packaged: str = os.path.join(self.EXPORT_DIR, self._get_mod_zip_name())
		export_packaged_path: str = os.path.join(self.GODASUM_MODS_DIR, self.modid.capitalize(), f"{self.modid.capitalize()}.zip")

		with zipfile.ZipFile(self.EXPORT_ZIP_PATH, 'r') as zf:
			print(f"Reading zip `{self.EXPORT_ZIP_PATH}`.")
			with tempfile.TemporaryDirectory(prefix=f"{self.modid.capitalize()}.zip_TEMPORARY_", dir=self.EXPORT_DIR) as td:
				print(f"Using `{td}` as temporary directory.")
				print("Extracting...")
				zf.extractall(td)
				print("Extracted.")

				# Now, we clean it and remove unnecessary content.
				# Remove source directory, we don't need it. The core game already has it.
				# shutil.rmtree(os.path.join(td, "Godasum"))
				# We don't need to delete it if the mod_export already ignores it.

				# Delete unnecessary dlls in both configurations.
				self._delete_unneeded_dlls(td)

				print("Saving...")
				with zipfile.ZipFile(export_packaged_path, 'w') as zfout:
					for root, dirs, files in os.walk(td):
						for file in files:
							# zfout.write(os.path.join(root, file))
							zfout.write(
								os.path.join(root, file),
								os.path.relpath(
									os.path.join(root, file),
									os.path.join(td)
								)
							)
				print(f"Saving to `{export_packaged_path}`.")

			# Now, copy over our dlls.
			os.makedirs(os.path.join(mod_user_path, "dll"), exist_ok=True)
			print(f"Doing mono: {self.MONO_BIN_DIR}")
			for _f in os.listdir(self.MONO_BIN_DIR):
				if not _f.endswith(".dll"):
					continue

				skip_it: bool = False
				for deletable in self.DELETABLE_DLLS:
					if _f == f"{deletable}.dll":
						skip_it: bool = True
						break
				if skip_it:
					continue

				f = os.path.join(self.MONO_BIN_DIR, _f)
				if not os.path.isfile(f):
					continue
				print(f"f: {f}")

				shutil.copy2(
					f,
					os.path.join(mod_user_path, "dll", _f)
				)

			# Now, finally, copy over our modinfo.json
			shutil.copy2(
				self.MODINFO_PATH,
				os.path.join(mod_user_path, "modinfo.json")
			)

	def copy_to_mods(self):
		raise NotImplementedError()
		src: str = os.path.join(self.EXPORT_DIR, self._get_mod_zip_name())
		dest: str = os.path.join(self.GODASUM_MODS_DIR, self._get_mod_zip_name())
		print(f"Copying from `{src}`...")
		print(f"...to `{dest}`.")

		shutil.copy2(
			src,
			dest
		)

	def export_godot(self):
		"""Tells Godot to export the mod as a zip file.

		After calling this, you should package it.
		"""
		proc = subprocess.run(
			[
				self.GODOT_PATH,
				"--path",
				self._wsl_to_win_path(os.path.join(self.REPO_DIR, "project.godot")),
				"--no-window",
				"--quit",
				"--export-pack",
				"mod_export",
				self._wsl_to_win_path(os.path.join(self.BUILD_DIR, "export", f"{self.modid.capitalize()}.zip"))
			],
			stdout=subprocess.PIPE,
			stderr=subprocess.PIPE,
			text=True
		)
		print(proc.stdout)
		if proc.returncode != 0:
			print("Error with exporting.")
			print("Stdout:")
			print(proc.stdout)
			print("Stderr:")
			print(proc.stderr)
			exit()

		print("Successful export.")

	def build_dll(self):
		"""Build the DLL project."""
		csproj_path: str = os.path.join(self.REPO_DIR, f"{self.modid.capitalize()}.csproj")
		if self._in_wsl():
			csproj_path = self._wsl_to_win_path(csproj_path)

		proc = subprocess.run(
			[
				"dotnet.exe",
				"build",
				csproj_path,
				# TODO
				"/property:Configuration=Debug"
			],
			stdout=subprocess.PIPE,
			stderr=subprocess.PIPE,
			text=True
		)
		print(proc.stdout)
		if proc.returncode != 0:
			print("Error with exporting.")
			print("Stdout:")
			print(proc.stdout)
			print("Stderr:")
			print(proc.stderr)
			exit()

		print("Successful DLL build.")
		print("Copying.")
		# TODO: needs testing
		shutil.copy2(
			os.path.join(self.MONO_BIN_DIR, f"{self.modid.capitalize()}.dll"),
			os.path.join(self.GODASUM_MODS_DIR, f"{self.modid.capitalize()}", "dll", f"{self.modid.capitalize()}.dll")
		)

	def _read_modinfo(self):
		content: str
		with open(self.MODINFO_PATH, 'r') as f:
			content = f.read()

		data = json.loads(content)

		self.modinfo = data
		self.modid = data["modid"]

	def _read_paths(self):
		content: str
		with open(self.PATHS_PATH, 'r') as f:
			content = f.read()

		data = json.loads(content)
		self.GODASUM_MODS_DIR = data["godasum_mods_dir"]
		self.GODOT_PATH = data["godot_path"]

		# But wait, we need to do some converting to/from WSL here.
		if self._in_wsl():
			self.GODASUM_MODS_DIR = self._get_wsl_path(self.GODASUM_MODS_DIR)
			self.GODOT_PATH = self._get_wsl_path(self.GODOT_PATH)

	def _get_wsl_path(self, path: str) -> str:
		result = subprocess.run(
			[
				"wslpath",
				"-u",
				path
			],
			stdout=subprocess.PIPE,
			stderr=subprocess.PIPE,
			text=True
		)
		if result.returncode != 0:
			print(f"Error. Tried to convert Windows path (from paths.json) to WSL path, because I thought we were running in WSL. (path={path})")
			print("Dumping `wslpath` stdout:")
			print(result.stdout)
			print("Stderr:")
			print(result.stderr)
			exit()
		return result.stdout.strip()

	def _wsl_to_win_path(self, path: str) -> str:
		result = subprocess.run(
			[
				"wslpath",
				"-w",
				path
			],
			stdout=subprocess.PIPE,
			stderr=subprocess.PIPE,
			text=True
		)
		if result.returncode != 0:
			print(f"Error. Tried to convert WSL path to Windows. (path={path})")
			print("Dumping `wslpath` stdout:")
			print(result.stdout)
			print("Stderr:")
			print(result.stderr)
			exit()
		return result.stdout.strip()

	def _get_mod_zip_name(self) -> str:
		if self._cached_mod_zip_name is None:
			self._cached_mod_zip_name = f"{self.modid.capitalize()}-OUT.zip"
		return self._cached_mod_zip_name

	def _in_wsl(self) -> bool:
		if self._cached_in_wsl is None:
			import platform
			data = platform.uname()
			if (
				data.system.lower() == "linux"
				and
				("microsoft" in data.release.lower())
				and
				("microsoft" in data.version.lower())
			):
				self._cached_in_wsl = True
			else:
				self._cached_in_wsl = False
		return self._cached_in_wsl

	def _delete_unneeded_dlls(self, td: str):
		print("Deleting unneeded dlls (these DLLS should be provided by Godasum)...")
		assemblies_dir: str = os.path.join(td, ".mono", "assemblies")
		release_dir: str = os.path.join(assemblies_dir, "Release")
		debug_dir: str = os.path.join(assemblies_dir, "Debug")

		if os.path.isdir(release_dir):
			print("Handling release directory...")
			self._delete_unneeded_dlls_in(release_dir)

		if os.path.isdir(debug_dir):
			print("Handling debug directory...")
			self._delete_unneeded_dlls_in(debug_dir)

		print("Finished deleting unneeded dlls.")

	def _delete_unneeded_dlls_in(self, dir: str):
		for deletable in self.DELETABLE_DLLS:
			deletable_path = os.path.join(
				dir,
				f"{deletable}.dll"
			)

			if os.path.isfile(deletable_path):
				print(f"Removing `{deletable_path}`.")
				os.remove(deletable_path)

def main():
	p: Packager = Packager().start()

	actions: dict = {
		"export": p.export_godot,
		"package": p.package_zip,
		"build_dll": p.build_dll
	}

	def print_actions():
		print("List of actions:")
		for a in actions:
			if a.startswith("_"): continue
			print(f"\t{a}")

	if len(sys.argv) < 2:
		print("Error: specify an action")
		print_actions()
		exit()

	# The 0th element is the callpath.
	action: str = sys.argv[1].lower()

	# if action == "export":
	# 	p.export_godot()
	# elif action == "cleanzip":
	# 	p.clean_zip()
	# else:
	# 	print("No action.")
	# 	exit()

	if action in actions:
		actions[action]()
	else:
		print(f"No action named {action}.")
		print_actions()
		exit()


if __name__ == "__main__":
	main()
