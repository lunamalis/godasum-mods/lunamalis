#!/usr/bin/env bash
SCRIPT_DIR="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
# REPO_DIR="${SCRIPT_DIR}/.."
REPO_DIR="$(dirname "${SCRIPT_DIR}")"


echo "WARNING: This file is untested, but should work."
echo "If you are running WSL on Windows, try using link.wsl.bash instead."
echo ""

echo "Do not end with a trailing slash."
read -ep "Enter Godasum DLL directory (required): " godasum_dll_dir

godasum_dll_real_dir="$( cd "$(dirname "${godasum_dll_dir}}")" >/dev/null 2>&1 ; pwd -P )/$(basename "${godasum_dll_dir}")"
echo "${godasum_dll_real_dir}"

if [[ ! -d "${godasum_dll_real_dir}" ]]; then
	echo "Given directory does not seem to exist."
	exit 1
fi

if [[ ! -d "${REPO_DIR}/lib" ]]; then
	echo "Making ${REPO_DIR}/lib"
	mkdir "${REPO_DIR}/lib"
fi

if [[ -f "${REPO_DIR}/lib/Godasum" ]]; then
	echo "Symlink to DLLs already exist."
	echo "You can safely delete them yourself, but double-check them to make sure"
	echo "you are not deleting the wrong thing."
	exit 1
fi
ln -s "${godasum_dll_real_dir}" "${REPO_DIR}/lib/Godasum"

echo ""
echo "This directory should contain Godasum.csproj."
echo "Do not end with a trailing slash."
read -ep "Enter Godasum source directory (optional, press enter to skip): " godasum_source_dir

if [[ -z "${godasum_source_dir}" ]]; then
	echo "Skipping source dir link."
	exit 0
fi

godasum_source_real_dir="$( cd "$(dirname "${godasum_source_dir}}")" >/dev/null 2>&1 ; pwd -P )/$(basename "${godasum_source_dir}")"
echo "${godasum_source_real_dir}"

if [[ ! -d "${godasum_source_real_dir}" ]]; then
	echo "Given directory does not seem to exist."
	exit 1
fi

if [[ ! -f "${godasum_source_real_dir}/Godasum.csproj" ]]; then
	echo "Given directory does not contain `Godasum.csproj` file."
	exit 1
fi

if [[ ! -d "${godasum_source_real_dir}/Godasum" ]]; then
	echo "Given directory not matching correct structure."
	echo "Should look like:"
	echo "\t${godasum_source_real_dir}"
	echo "\t\tGodasum/"
	echo "\t\t\tsource etc..."
	echo "\t\tGodasum.csproj"
	exit 1
fi

echo "${godasum_source_real_dir}/Godasum <-> ${REPO_DIR}/Godasum"
ln -s "${godasum_source_real_dir}/Godasum" "${REPO_DIR}/Godasum"
