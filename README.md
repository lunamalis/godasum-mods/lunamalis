# Lunamalis

## Compiling

Compiling has three stages. The first two are done *once*, while the third
stage is done whenever needed.


To compile, you will need the following tools:

* [Godot Engine v3.2.1.stable.mono.official](https://godotengine.org/download/windows)
 ** [Direct Windows 64bit download](https://downloads.tuxfamily.org/godotengine/3.2.1/mono/Godot_v3.2.1-stable_mono_win64.zip)
* [Dotnet SDK 3.1.201](https://dotnet.microsoft.com/download/dotnet-core/3.1)
 ** [Direct Windows 64bit download](https://download.visualstudio.microsoft.com/download/pr/56131147-65ea-47d6-a945-b0296c86e510/44b43b7cb27d55081e650b9a4188a419/dotnet-sdk-3.1.201-win-x64.exe)
* Python 3.7 or higher
 ** [Direct Windows 64bit installer download for Python 3.7.5](https://www.python.org/ftp/python/3.7.0/python-3.7.0-amd64.exe)

## State 1

Before compiling, you will need to link to Godasum. To do this, you will have
to download Godasum first.

Visit [Godasum's Gitlab page](https://gitlab.com/lunamalis/godasum) and either
press download or clone it to a directory of your choice.

Next, you will have to link this project to Godasum. Doing this *should not*
modify Godasum at all. To do this, run the linking script in the `scripts`
directory.

If you are on Windows, run `link.bat`. Otherwise, run `link.bash`. Users using
WSL can use `link.wsl.bash`.

Once this is finished, you should have two symlinks that point towards Godasum.
You will have a directory inside your project that is named `Godasum`. This
allows you to reference Godasum when using Godot assets.

The second symlink is inside `lib/Godasum`, which points towards Godasum's
DLLs.

## Stage 2

The next step is to make your `paths.json` file. This is very easy. Check out
`paths.json.example` for an example of how to set it up. You need it to point
towards two locations:

* Your mods directory
* Your Godot executable path

Again, check out `paths.json.example` for how to set this up.

### Stage 3

Compiling is done by using the `build.bash` script in the `scripts` directory.

To build the project from scratch, run the following:

1. `build.bash export`
2. `build.bash package`
3. `build.bash build_dll`

Whenever you edit any `.tscn` files (or other Godot data), you must run
`build.bash export` and `build.bash package` again.

Whenever you edit any `.cs` files (or other C# data), you must run
`build.bash build_dll` again.

The `export` action tells Godot to build the project, and export a zip file
into `build/export/PROJECTNAME.zip`. However, this alone is not enough for
Godasum to accept this as a mod.

The `package` action will take the exported zip from the `export` action, and
send to Godasum's mod directory. It would be sent to
`%GODASUM_USER_DIR%/mods/MODNAME/MODNAME.zip`

`%GODASUM_USER_DIR%` can be found in different locations depending upon your
operating system. Consult this:

**Operating system**|**Location**
:-----|:-----
Windows|`%APPDATA%/Roaming/Godasum`
MacOS|`~/Library/Application Support/Godasum`
Linux|`~/.local/share/Godasum`

On Windows, `%APPDATA%` is usually located at `C:/Users/YOURNAME/AppData`.

On MacOS and Linux, `~` refers to your home directory.
