namespace Lunamalis.Universe.Features {
	using System.Collections.Generic;
	using System.Diagnostics;
	using System.Linq;
	using Godasum.universe;
	using Lunamalis.Universe.Loci;
	using Newtonsoft.Json;

	public class HyperlaneConnection : UniverseFeature
	{

		[JsonProperty("from")]
		public int From { get; set; }

		[JsonProperty("to")]
		public int To { get; set; }

		public HyperlaneConnection Generate(AbstractLunamalisLocus first, AbstractLunamalisLocus second) {
			this.From = first.InstanceId;
			this.To = second.InstanceId;

			if (first.Connections is null) {
				first.Connections = new int[0];
			}
			if (second.Connections is null) {
				second.Connections = new int[0];
			}

			first.Connections.Append(this.InstanceId);
			second.Connections.Append(this.InstanceId);

			return this;
		}

		public override void Spawn(Godot.Node parent) {
			var node = new HyperlaneConnectionCosmic() {
				Parent = this
			};
			parent.AddChild(node);
		}

		public static bool IsConnected(List<HyperlaneConnection> hyperlanes, AbstractLunamalisLocus first, AbstractLunamalisLocus second) {
			if (first.Connections is null || second.Connections is null) {
				return false;
			}

			// foreach (var connection in first.Connections) {
			// 	// var hyperlane = (HyperlaneConnection)data.Features[connection];

			// 	if (second.Connections.Contains(connection)) {
			// 		return true;
			// 	}

			// }

			foreach (var hl in hyperlanes) {
				if (
					(
						hl.From == first.InstanceId
						&&
						hl.To == second.InstanceId
					)
					||
					(
						hl.From == second.InstanceId
						&&
						hl.To == first.InstanceId
					)
				) {
					return true;
				}
			}

			return false;
		}

		public class HyperlaneConnectionCosmic : Godot.Node2D
		{
			public HyperlaneConnection? Parent;

			public override void _Draw()
			{
				Debug.Assert(this.Parent != null);

				var from = (AbstractLunamalisLocus)this.Parent.UniverseData.Features[this.Parent.From];
				var to = (AbstractLunamalisLocus)this.Parent.UniverseData.Features[this.Parent.To];

				this.DrawLine(
					from.Position,
					to.Position,
					new Godot.Color(1.0f, 1.0f, 1.0f, 0.3f),
					2,
					true
				);
			}

		}

	}
}
