namespace Lunamalis.Universe {
	using System;
	using System.Collections.Generic;
	using System.Collections.ObjectModel;
	using System.Diagnostics;
	using System.Linq;
	using Godasum.universe;

	public class UniverseGenerator : Godasum.universe.generation.AbstractUniverseGenerator
	{

		public override UniverseData Generate(Random random) {
			var ud = new UniverseData();

			var features = new List<IUniverseFeature>();

			var loci = new List<Loci.AbstractLunamalisLocus>();
			const int STAR_COUNT = 100;
			const float RANGE = 1000;
			for (int i = 0; i < STAR_COUNT; i++) {
				float x = (float)random.NextDouble() * RANGE - RANGE / 2;
				float y = (float)random.NextDouble() * RANGE - RANGE / 2;

				var locus = this.CreateFeature<Loci.Star>();
				locus.Position = new Godot.Vector2(x, y);
				loci.Add(locus);
			}
			features.AddRange(loci);

			// Here we generate hyperlanes.
			var queued_stars = loci.ToList();
			var tmp_hyperlanes = new List<Features.HyperlaneConnection>();
			while (queued_stars.Count > 0) {
				// Choose the last star.
				var last = queued_stars[^1];

				// Choose the closest star.
				float shortest_distance = float.MaxValue;
				Loci.AbstractLunamalisLocus? closest = null;
				foreach (var star in loci) {
					if (ReferenceEquals(star, last)) {
						continue;
					}

					if (Features.HyperlaneConnection.IsConnected(tmp_hyperlanes, last, star)) {
						continue;
					}

					var distance = last.Position.DistanceTo(star.Position);
					if (distance < shortest_distance) {
						shortest_distance = distance;
						closest = star;
					}
				}

				if (closest is null) {
					queued_stars.RemoveAt(queued_stars.Count - 1);
					continue;
				}

				// Make the hyperlane.
				var hyperlane = this.CreateFeature<Features.HyperlaneConnection>().Generate(last, closest);
				features.Add(hyperlane);
				tmp_hyperlanes.Add(hyperlane);

				queued_stars.RemoveAt(queued_stars.Count - 1);
			}
			tmp_hyperlanes.Clear();

			//

			// ud.Loci = loci.ToArray();
			ud.Features = features.ToArray();

			return ud;
		}

		// public override void Load() {
		// 	throw new System.NotImplementedException();
		// }

		// public override void Spawn(Godasum.universe.Universe universe, Godot.Node node) {
		// 	foreach (var locus in universe.Data.Loci) {
		// 		Godot.GD.Print("Spawning locus.");
		// 		var outer = locus.OuterLocus;
		// 		node.AddChild(outer);

		// 		foreach (var feature in locus.Features) {
		// 			Godot.GD.Print("Feature.");
		// 			feature.Spawn(node);
		// 		}
		// 	}
		// }

		private T[][] _get_pairs_of<T>(IReadOnlyList<T> original) {
			var objects = original.ToArray();

			// We shuffle it, then split into chunks.
			int count = objects.Length;
			while (count > 0) {
				int k = Godasum.util.RandomUtil.rnd.Next(count--);
				var temporary = objects[count];
				objects[count] = objects[k];
				objects[k] = temporary;
			}

			// And then split into chunks.
			int chunks = objects.Length / 2;
			int remainder = objects.Length % 2;

			var returner = new T[chunks + remainder][];
			int offset = 0;

			for (int i = 0; i < chunks; i++) {
				returner[i] = new ArraySegment<T>(objects, offset, 2).ToArray();
				offset += 2;
			}

			if (remainder > 0) {
				var last = new ArraySegment<T>(objects, offset, remainder).ToArray();
				returner[^1] = last;
			}

			return returner;

		}

	}
}
