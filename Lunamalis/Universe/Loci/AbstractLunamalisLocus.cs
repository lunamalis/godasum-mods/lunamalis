namespace Lunamalis.Universe.Loci {
	using Godasum.universe;
	using Newtonsoft.Json;

	public abstract class AbstractLunamalisLocus : AbstractLocus
	{

		[JsonProperty("connections")]
		public int[]? Connections { get; set; } = null;


	}
}
