namespace Lunamalis.Universe.Loci {
	using System;

	public sealed class Star : AbstractLunamalisLocus
	{

		public float Mass { get; set; } = 25.0f;

		public float Age { get; set; } = 25.0f;

		public override void Spawn(Godot.Node node) {
			var star = new StarOuter() {
				Position = this.Position
			};
			node.AddChild(star);
		}

		public sealed class StarOuter : Godot.Node2D
		{

			// TODO: this should be calculated from mass, age...
			public float Radius { get; set; } = 2.7f;//5.0f;

			// public Godot.Color Color { get; set; } = new Godot.Color(1.0f, 1.0f, 0.2f);
			public Godot.Color Color { get; set; } = new Godot.Color(247 / 255.0f, 239 / 255.0f, 186 / 255.0f);

			public float Mass { get; set; } = 2.00f;

			public override void _Draw() {
				const float MAX_ANGLE = MathF.PI * 2;
				const float OFFSET_ANGLE = MathF.PI / 2;
				const int POINT_COUNT = 16;

				var points = new Godot.Vector2[POINT_COUNT];

				for (int i = 0; i < POINT_COUNT; i++) {
					float angle = MAX_ANGLE * i / POINT_COUNT + OFFSET_ANGLE;
					points[i] = new Godot.Vector2(MathF.Cos(angle), MathF.Sin(angle)) * this.Radius;
				}

				this.DrawColoredPolygon(points, this.Color);
			}

		}

	}
}
