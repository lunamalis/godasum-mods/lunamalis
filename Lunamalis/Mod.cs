﻿namespace Lunamalis {
	public class Mod : Godasum.modding.Mod
	{
		public override string MODID => "lunamalis";

		public override void Setup() {
			Godot.GD.Print("Lunamalis setting up.");
			// this.register_entity<BasicLunaEntity>();

			// this.register_entity<BasicLunaEntity>("basic_luna_entity");
			// // this.register_entity<BasicLunaShip>("basic_luna_ship");

			// GD.Print($"Basic Luna ship data: {BasicLunaShip.SCENE_PATH}");
			// this.register_ship<BasicLunaShip>("basic_luna_ship");


			// this.register_projectile<BasicProj>("basic_luna_proj");
			// this.register_ship<AngryShip>("angry_ship");
			// this.register_ship<GreenShip>("green_ship");
			// this.register_ship<SpeedyShip>("speedy_ship");
			// this.register_entity<ships.AngryShip>("angry_ship");

			this.RegisterUniverseFeatures();

			Godasum.scenes.starmap.Starmap.UniverseGenerator = new Universe.UniverseGenerator();

		}

		internal void RegisterUniverseFeatures() {
			var registry = Godasum.Core.Registry.UniverseFeatureRegistry;

			registry.Register<Universe.Loci.Star>();
			registry.Register<Universe.Features.HyperlaneConnection>();
		}

	}
}
