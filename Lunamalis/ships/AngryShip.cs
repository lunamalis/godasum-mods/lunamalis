namespace Lunamalis.ships {
	using Godot;
	using System;

	public class AngryShip : Godasum.objects.battle.ShipBO
	{

		public override string EntityId => "angry_ship";

		public override string ScenePath => "source/Lunamalis/ships/AngryShip.tscn";

	}
}
